import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tiktok_clone/constants/gaps.dart';
import 'package:tiktok_clone/constants/sizes.dart';
import 'package:tiktok_clone/features/authentication/view_models/signup_view_model.dart';
import 'package:tiktok_clone/features/widgets/form_button.dart';

class BirthdayScreen extends ConsumerStatefulWidget {
  const BirthdayScreen({
    super.key,
  });

  @override
  BirthdayScreenScreenState createState() => BirthdayScreenScreenState();
}

class BirthdayScreenScreenState extends ConsumerState<BirthdayScreen> {
  final TextEditingController _BirthdayScreenController =
      TextEditingController();
  DateTime initialDate =
      DateTime.now().subtract(const Duration(days: (365 * 12)));

  @override
  void initState() {
    super.initState();
    _setTextFieldDate(initialDate);
  }

  @override
  void dispose() {
    _BirthdayScreenController.dispose();
    super.dispose();
  }

  void _onNextTap() {
    // context.pushReplacementNamed(
    //     InterestsScreen.routeName); // pushReplacementNamed 뒤로가기를 사용할 수 없음
    //context.goNamed(InterestsScreen.routeName); // goNamed 기존 스텍 삭제
    final String bio = _BirthdayScreenController.text;
    final state = ref.read(signUpForm.notifier).state;
    ref.read(signUpForm.notifier).state = {
      ...state,
      "bio": bio,
    };

    ref.read(signUpProvider.notifier).signUp(context);
  }

  void _onScaffoldTap() {
    FocusScope.of(context).unfocus();
  }

  void _setTextFieldDate(initalDate) {
    final textDate = initalDate.toString().split(" ").first;
    _BirthdayScreenController.value = TextEditingValue(text: textDate);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onScaffoldTap,
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            "Sign up",
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: Sizes.size36),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Gaps.v40,
              const Text(
                "When's your birthday?",
                style: TextStyle(
                  fontSize: Sizes.size24,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Gaps.v8,
              const Text(
                "Your birthday won't be shown publicly.",
                style: TextStyle(
                  fontSize: Sizes.size16,
                  color: Colors.black45,
                ),
              ),
              Gaps.v16,
              TextField(
                controller: _BirthdayScreenController,
                keyboardType: TextInputType.name,
                autocorrect: false,
                onEditingComplete: _onNextTap,
                decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.grey.shade400,
                    ),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.grey.shade400,
                    ),
                  ),
                ),
                cursorColor: Theme.of(context).primaryColor,
              ),
              Gaps.v16,
              GestureDetector(
                onTap: _onNextTap,
                child: FormButton(
                  text: "Next",
                  disabled: ref.watch(signUpProvider).isLoading,
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          child: SizedBox(
            height: 300,
            child: CupertinoDatePicker(
              maximumDate: initialDate,
              initialDateTime: initialDate,
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: _setTextFieldDate,
            ),
          ),
        ),
      ),
    );
  }
}
