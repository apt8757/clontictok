import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tiktok_clone/constants/gaps.dart';
import 'package:tiktok_clone/constants/sizes.dart';
import 'package:tiktok_clone/features/videos/views/video_preview_screen.dart';

class VideoRecordingScreen extends StatefulWidget {
  static const routeURL = "/upload";
  static const routeName = "postVideo";
  const VideoRecordingScreen({super.key});

  @override
  State<VideoRecordingScreen> createState() => _VideoRecordingScreenState();
}

class _VideoRecordingScreenState extends State<VideoRecordingScreen>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  late final AnimationController _buttonAnimationController =
      AnimationController(
    vsync: this,
    duration: const Duration(milliseconds: 200),
  );

  late final AnimationController _progressAnimationController =
      AnimationController(
    vsync: this,
    duration: const Duration(seconds: 10),
    lowerBound: 0.0,
    upperBound: 1.0,
  );

  late final Animation<double> _buttonAnimation =
      Tween(begin: 1.0, end: 1.3).animate(_buttonAnimationController);

  late CameraController _cameraController;
  late FlashMode _flashMode;
  late double _minZoomLevel;
  late double _maxZoomLevel;
  double _zoomLevel = 0.0;
  bool _hasPermission = false;
  bool _selfieMode = false;

  Future<void> _toggleSelfieMode() async {
    _selfieMode = !_selfieMode;
    initCamera();
    setState(() {});
  }

  Future<void> _setFlashMode(FlashMode newFlashMode) async {
    await _cameraController.setFlashMode(newFlashMode);
    _flashMode = newFlashMode;
    setState(() {});
  }

  Future<void> initCamera() async {
    final cameras = await availableCameras();
    //print(cameras);
    if (cameras.isEmpty) {
      return;
    }
    _cameraController = CameraController(
      cameras[_selfieMode ? 1 : 0],
      ResolutionPreset.ultraHigh,
    );
    await _cameraController.initialize();
    await _cameraController.prepareForVideoRecording();

    _minZoomLevel = await _cameraController.getMinZoomLevel();
    _maxZoomLevel = await _cameraController.getMaxZoomLevel();

    _flashMode = _cameraController.value.flashMode;

    _zoomLevel = _minZoomLevel;
    await _cameraController.setZoomLevel(_minZoomLevel);
    setState(() {});
  }

  Future<void> initPermissions() async {
    final cameraPermission = await Permission.camera.request();
    final micPermission = await Permission.microphone.request();

    final cameraDenied =
        cameraPermission.isDenied || cameraPermission.isPermanentlyDenied;
    final micDenied =
        micPermission.isDenied || micPermission.isPermanentlyDenied;

    if (!cameraDenied && !micDenied) {
      _hasPermission = true;
      await initCamera();
    }
  }

  Future<void> _starRecording(TapDownDetails _) async {
    if (_cameraController.value.isRecordingVideo) {
      return;
    }

    await _cameraController.startVideoRecording();

    _buttonAnimationController.forward();
    _progressAnimationController.forward();
  }

  Future<void> _stopRecording() async {
    if (!_cameraController.value.isRecordingVideo) {
      return;
    }

    _buttonAnimationController.reverse();
    _progressAnimationController.reset();

    final video = await _cameraController.stopVideoRecording();

    if (!mounted) return;

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => VideoPreviewScreen(
          video: video,
          isPicked: true,
        ),
      ),
    );
  }

  Future<void> _videoZoomInOut(DragUpdateDetails details) async {
    //print(details.delta.dy);
    double deltaY = details.delta.dy;
    if (deltaY > 0) {
      _zoomLevel =
          _zoomLevel <= _minZoomLevel ? _minZoomLevel : _zoomLevel - 0.05;
    } else if (deltaY < 0) {
      _zoomLevel =
          _zoomLevel >= _maxZoomLevel ? _maxZoomLevel : _zoomLevel + 0.05;
    } else {
      return;
    }
    await _cameraController.setZoomLevel(_zoomLevel);
    setState(() {});
  }

  Future<void> _onPickVideoPressed() async {
    final video = await ImagePicker().pickVideo(
      source: ImageSource.gallery,
    );
    if (video == null) {
      return;
    }

    if (!mounted) return;

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => VideoPreviewScreen(
          video: video,
          isPicked: false,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initPermissions();
    WidgetsBinding.instance.addObserver(this);

    _progressAnimationController.addListener(() {
      setState(() {});
    });
    _progressAnimationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _stopRecording();
      }
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (!_hasPermission) return;
    if (!_cameraController.value.isInitialized) return;
    if (state == AppLifecycleState.inactive) {
      _cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      initCamera();
    }
  }

  @override
  void dispose() {
    _cameraController.dispose();
    _buttonAnimationController.dispose();
    _progressAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.black,
          body: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: !_hasPermission || !_cameraController.value.isInitialized
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const [
                      Text(
                        "카메라 초기화 중...",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: Sizes.size20,
                        ),
                      ),
                      Gaps.v20,
                      CircularProgressIndicator.adaptive(),
                    ],
                  )
                : Stack(
                    alignment: Alignment.center,
                    children: [
                      CameraPreview(_cameraController),
                      const Positioned(
                        top: Sizes.size20,
                        left: Sizes.size10,
                        child: CloseButton(
                          color: Colors.white,
                        ),
                      ),
                      Positioned(
                        top: Sizes.size40,
                        right: Sizes.size10,
                        child: Column(
                          children: [
                            IconButton(
                              onPressed: _toggleSelfieMode,
                              icon: const Icon(
                                Icons.cameraswitch,
                                color: Colors.white,
                              ),
                            ),
                            Gaps.v10,
                            IconButton(
                              onPressed: () => _setFlashMode(FlashMode.off),
                              icon: Icon(
                                Icons.flash_off_rounded,
                                color: _flashMode == FlashMode.off
                                    ? Colors.amber.shade200
                                    : Colors.white,
                              ),
                            ),
                            Gaps.v10,
                            IconButton(
                              onPressed: () => _setFlashMode(FlashMode.always),
                              icon: Icon(
                                Icons.flash_on_rounded,
                                color: _flashMode == FlashMode.always
                                    ? Colors.amber.shade200
                                    : Colors.white,
                              ),
                            ),
                            Gaps.v10,
                            IconButton(
                              onPressed: () => _setFlashMode(FlashMode.auto),
                              icon: Icon(
                                Icons.flash_auto_rounded,
                                color: _flashMode == FlashMode.auto
                                    ? Colors.amber.shade200
                                    : Colors.white,
                              ),
                            ),
                            Gaps.v10,
                            IconButton(
                              onPressed: () => _setFlashMode(FlashMode.torch),
                              icon: Icon(
                                Icons.flashlight_on_rounded,
                                color: _flashMode == FlashMode.torch
                                    ? Colors.amber.shade200
                                    : Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        width: MediaQuery.of(context).size.width,
                        bottom: Sizes.size40,
                        child: Row(
                          children: [
                            const Spacer(),
                            GestureDetector(
                              onTapDown: _starRecording,
                              onTapUp: (details) => _stopRecording(),
                              onPanUpdate: (details) =>
                                  _videoZoomInOut(details),
                              onPanEnd: (details) => _stopRecording(),
                              child: ScaleTransition(
                                scale: _buttonAnimation,
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    SizedBox(
                                      width: Sizes.size80 + Sizes.size14,
                                      height: Sizes.size80 + Sizes.size14,
                                      child: CircularProgressIndicator(
                                        color: Colors.red.shade400,
                                        strokeWidth: Sizes.size6,
                                        value:
                                            _progressAnimationController.value,
                                      ),
                                    ),
                                    Container(
                                      width: Sizes.size80,
                                      height: Sizes.size80,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.red.shade400,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                alignment: Alignment.center,
                                child: IconButton(
                                    onPressed: _onPickVideoPressed,
                                    icon: const FaIcon(
                                      FontAwesomeIcons.image,
                                      color: Colors.white,
                                    )),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
          )),
    );
  }
}
