import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:tiktok_clone/constants/gaps.dart';
import 'package:tiktok_clone/constants/sizes.dart';

class VideoBgm extends StatelessWidget {
  const VideoBgm({
    super.key,
    required this.bgmInfo,
  });

  final String bgmInfo;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 30,
      width: MediaQuery.of(context).size.width / 3,
      child: Row(
        children: [
          const Text(
            '♫',
            style: TextStyle(
              color: Colors.white,
              fontSize: Sizes.size16,
            ),
          ),
          Gaps.h10,
          Expanded(
            child: Marquee(
              text: bgmInfo,
              style: const TextStyle(
                color: Colors.white,
              ),
              blankSpace: 100,
              velocity: 15,
            ),
          ),
        ],
      ),
    );
  }
}
