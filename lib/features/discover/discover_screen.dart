import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tiktok_clone/constants/breakpoints.dart';
import 'package:tiktok_clone/constants/gaps.dart';
import 'package:tiktok_clone/constants/sizes.dart';
import 'package:tiktok_clone/utils/utils.dart';

final tabs = [
  "Top",
  "Users",
  "Videos",
  "Sounds",
  "LIVE",
  "Shopping",
  "Brands",
];

class DiscoverScreen extends StatefulWidget {
  const DiscoverScreen({super.key});

  @override
  State<DiscoverScreen> createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {
  final TextEditingController _textEditingController = TextEditingController();

  bool _isWriting = false;

  void _onSearchChanged(String value) {
    print(value);
  }

  void _onSearchSubmitted(String value) {
    print(value);
  }

  void _stopWriting() {
    FocusScope.of(context).unfocus();
    setState(() {
      _isWriting = false;
    });
  }

  void _onXmarkClearTap() {
    _textEditingController.clear();
    setState(() {
      _isWriting = false;
    });
  }

  void _onStartWriting() {
    setState(() {
      _isWriting = true;
    });
  }

  void _onBackTap(BuildContext context) {
    print("뒤로가기 버튼");
  }

  @override
  void deactivate() {
    // _textEditingController.dispose();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: _stopWriting,
      child: DefaultTabController(
        length: tabs.length,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            elevation: 1,
            title: ConstrainedBox(
              constraints: const BoxConstraints(
                maxWidth: Breakpoints.sm,
              ),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () => _onBackTap(context),
                    child: const FaIcon(
                      FontAwesomeIcons.chevronLeft,
                    ),
                  ),
                  Gaps.h20,
                  Expanded(
                    child: TextField(
                      controller: _textEditingController,
                      onTap: _onStartWriting,
                      onChanged: _onSearchChanged,
                      onSubmitted: _onSearchSubmitted,
                      decoration: InputDecoration(
                        prefixIcon: const Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: Sizes.size12,
                            horizontal: Sizes.size10,
                          ),
                          child: FaIcon(
                            FontAwesomeIcons.magnifyingGlass,
                            size: Sizes.size20,
                          ),
                        ),
                        prefixIconColor:
                            isDarkMode(context) ? Colors.white : Colors.black,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                            Sizes.size8,
                          ),
                          borderSide: BorderSide.none,
                        ),
                        filled: true,
                        fillColor: isDarkMode(context)
                            ? Colors.grey.shade800
                            : Colors.grey.shade200,
                        contentPadding: const EdgeInsets.symmetric(
                          horizontal: Sizes.size12,
                        ),
                        hintText: "검색",
                        suffixIconConstraints: const BoxConstraints(
                          minWidth: 30,
                        ),
                        suffixIcon: Padding(
                          padding: const EdgeInsets.only(
                            right: Sizes.size14,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (_isWriting)
                                GestureDetector(
                                  onTap: _onXmarkClearTap,
                                  child: FaIcon(
                                    FontAwesomeIcons.solidCircleXmark,
                                    color: Colors.grey.shade600,
                                    size: Sizes.size16 + Sizes.size2,
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Gaps.h20,
                  const FaIcon(FontAwesomeIcons.sliders),
                  Gaps.h8,
                ],
              ),
            ),
            // 쿠퍼티노 텍스트필드
            // CupertinoSearchTextField(
            //   controller: _textEditingController,
            //   onChanged: _onSearchChanged,
            //   onSubmitted: _onSearchSubmitted,
            // ),
            bottom: TabBar(
              padding: const EdgeInsets.symmetric(horizontal: Sizes.size16),
              isScrollable: true,
              indicatorColor: Theme.of(context).tabBarTheme.indicatorColor,
              labelStyle: const TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: Sizes.size16,
              ),
              splashFactory: NoSplash.splashFactory,
              tabs: [
                for (var tab in tabs)
                  Tab(
                    text: tab,
                  )
              ],
            ),
          ),
          body: TabBarView(
            children: [
              GridView.builder(
                keyboardDismissBehavior:
                    ScrollViewKeyboardDismissBehavior.onDrag,
                itemCount: 20,
                padding: const EdgeInsets.all(
                  Sizes.size6,
                ),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: width > Breakpoints.lg ? 5 : 2,
                  crossAxisSpacing: Sizes.size10,
                  mainAxisSpacing: Sizes.size10,
                  childAspectRatio: 9 / 21,
                ),
                itemBuilder: (context, index) => LayoutBuilder(
                  builder: (context, constraints) => Column(
                    children: [
                      Container(
                        clipBehavior: Clip.hardEdge,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Sizes.size10),
                        ),
                        child: AspectRatio(
                          aspectRatio: 9 / 16,
                          child: FadeInImage.assetNetwork(
                              fit: BoxFit.cover,
                              placeholder: "assets/images/loading.gif",
                              image:
                                  "https://images.unsplash.com/photo-1555126634-323283e090fa?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1364&q=80"),
                        ),
                      ),
                      Gaps.v10,
                      const Text(
                        "면치기 면치기 야야야! 면치기 면치기 야야야! 면치기 면치기 야야야! ",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: Sizes.size16,
                          fontWeight: FontWeight.bold,
                          height: 1.1,
                        ),
                      ),
                      Gaps.v8,
                      if (constraints.maxWidth < 200 ||
                          constraints.maxWidth > 250)
                        DefaultTextStyle(
                          style: TextStyle(
                            color: isDarkMode(context)
                                ? Colors.grey.shade300
                                : Colors.grey.shade600,
                            fontWeight: FontWeight.w600,
                          ),
                          child: Row(
                            children: [
                              const CircleAvatar(
                                radius: 12,
                                backgroundImage: NetworkImage(
                                    "https://secure.gravatar.com/avatar/a7a4490fc6fd768832a922b4465863cc?s=800&d=identicon"),
                              ),
                              Gaps.h4,
                              const Expanded(
                                child: Text(
                                  "안녕 애들아 내가 누군지 아니?",
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              FaIcon(
                                FontAwesomeIcons.heart,
                                size: Sizes.size16,
                                color: Colors.grey.shade600,
                              ),
                              Gaps.h2,
                              const Text("2.5M"),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              ),
              for (var tabView in tabs.skip(1))
                Center(
                  child: Text(
                    tabView,
                    style: const TextStyle(
                      fontSize: Sizes.size28,
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
