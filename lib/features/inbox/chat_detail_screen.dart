import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tiktok_clone/constants/gaps.dart';
import 'package:tiktok_clone/constants/sizes.dart';

class ChatDetailScreen extends StatefulWidget {
  static const routeURL = ":chatId";
  static const routeName = "chatDetail";

  final String chatId;

  const ChatDetailScreen({
    super.key,
    required this.chatId,
  });

  @override
  State<ChatDetailScreen> createState() => _ChatDetailScreenState();
}

class _ChatDetailScreenState extends State<ChatDetailScreen> {
  final TextEditingController _textEditingController = TextEditingController();

  void _onStartWriting() {}

  void _stopWriting() {
    FocusScope.of(context).unfocus();
  }

  void _onChatChanged(String value) {
    print(value);
  }

  void _onChatSubmitted(String value) {
    print(value);
  }

  void _onSandTap() {}
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _stopWriting,
      child: Scaffold(
        appBar: AppBar(
          // elevation: 1,
          title: Stack(
            children: [
              ListTile(
                contentPadding: EdgeInsets.zero,
                horizontalTitleGap: Sizes.size8,
                leading: const CircleAvatar(
                  radius: Sizes.size24,
                  foregroundImage: NetworkImage(
                      "https://secure.gravatar.com/avatar/a7a4490fc6fd768832a922b4465863cc?s=800&d=identicon"),
                  child: Text("또알"),
                ),
                title: Text(
                  "또알 (${widget.chatId})",
                  style: const TextStyle(fontWeight: FontWeight.w600),
                ),
                subtitle: const Text("Active now"),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    FaIcon(
                      FontAwesomeIcons.flag,
                      // color: Colors.black,
                      size: Sizes.size20,
                    ),
                    Gaps.h32,
                    FaIcon(
                      FontAwesomeIcons.ellipsis,
                      // color: Colors.black,
                      size: Sizes.size20,
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 41,
                left: 30,
                child: Container(
                  padding: const EdgeInsets.all(Sizes.size9),
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                  ),
                ),
              ),
              Positioned(
                top: 43.5,
                left: 32.5,
                child: Container(
                  padding: const EdgeInsets.all(6.5),
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color.fromARGB(255, 93, 212, 97),
                  ),
                ),
              ),
            ],
          ),
        ),
        body: Stack(
          children: [
            ListView.separated(
              padding: const EdgeInsets.symmetric(
                vertical: Sizes.size20,
                horizontal: Sizes.size14,
              ),
              itemBuilder: (context, index) {
                var isMain = index % 2 == 0;
                return Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment:
                      isMain ? MainAxisAlignment.end : MainAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(Sizes.size14),
                      decoration: BoxDecoration(
                        color: isMain
                            ? Colors.blue
                            : Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(Sizes.size20),
                          topRight: const Radius.circular(Sizes.size20),
                          bottomLeft: Radius.circular(
                              isMain ? Sizes.size20 : Sizes.size5),
                          bottomRight: Radius.circular(
                              !isMain ? Sizes.size20 : Sizes.size5),
                        ),
                      ),
                      child: const Text(
                        "채팅하는 곳!",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: Sizes.size16,
                        ),
                      ),
                    ),
                  ],
                );
              },
              separatorBuilder: (context, index) => Gaps.v10,
              itemCount: 10,
            ),
            Positioned(
              bottom: 0,
              width: MediaQuery.of(context).size.width,
              child: BottomAppBar(
                color: Colors.grey.shade200,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: Sizes.size10,
                    horizontal: Sizes.size14,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 50,
                          child: TextField(
                            controller: _textEditingController,
                            onTap: _onStartWriting,
                            onChanged: _onChatChanged,
                            onSubmitted: _onChatSubmitted,
                            expands: true,
                            minLines: null,
                            maxLines: null,
                            textInputAction: TextInputAction.newline,
                            decoration: InputDecoration(
                              hintText: "Send a message...",
                              contentPadding: const EdgeInsets.symmetric(
                                horizontal: Sizes.size20,
                              ),
                              border: const OutlineInputBorder(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(Sizes.size20),
                                  topRight: Radius.circular(Sizes.size20),
                                  bottomLeft: Radius.circular(Sizes.size20),
                                ),
                                borderSide: BorderSide.none,
                              ),
                              filled: true,
                              fillColor: Colors.white,
                              suffixIcon: Padding(
                                padding:
                                    const EdgeInsets.only(left: Sizes.size14),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: const [
                                    FaIcon(
                                      FontAwesomeIcons.faceSmile,
                                      color: Colors.black,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Gaps.h10,
                      GestureDetector(
                        onTap: _onSandTap,
                        child: Container(
                          padding: const EdgeInsets.all(Sizes.size8),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.grey.shade300,
                          ),
                          child: const Center(
                            child: FaIcon(
                              FontAwesomeIcons.solidPaperPlane,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
