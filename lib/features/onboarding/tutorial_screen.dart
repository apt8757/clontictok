import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:tiktok_clone/constants/gaps.dart';
import 'package:tiktok_clone/constants/sizes.dart';
import 'package:tiktok_clone/utils/utils.dart';

enum Direction { rigth, left }

enum Page { first, second }

class TutorialScreen extends StatefulWidget {
  const TutorialScreen({super.key});

  @override
  State<TutorialScreen> createState() => _TutorialScreenState();
}

class _TutorialScreenState extends State<TutorialScreen> {
  Direction _direction = Direction.rigth;
  Page _showingPage = Page.first;

  void _onPanUpdate(DragUpdateDetails detail) {
    if (detail.delta.dx > 0) {
      setState(() {
        _direction = Direction.rigth;
      });
    } else {
      setState(() {
        _direction = Direction.left;
      });
    }
  }

  void _onPanEnd(DragEndDetails detail) {
    if (_direction == Direction.left) {
      setState(() {
        _showingPage = Page.second;
      });
    } else {
      setState(() {
        _showingPage = Page.first;
      });
    }
  }

  void _onEntertheAppTap() {
    //MainNavigationScreen()
    context.go("/home");
  }

// 페이지 부분 컴포넌트화 시키기

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: _onPanUpdate,
      onPanEnd: _onPanEnd,
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: Sizes.size24),
          child: SafeArea(
            child: AnimatedCrossFade(
              firstChild: const tutorialPage(
                gap: Gaps.v16,
                topGap: Gaps.v80,
                subject: "Watch cool videos!",
                subtitle:
                    "Videos are personalized for you based on what you watch, like, and share.",
              ),
              secondChild: const tutorialPage(
                gap: Gaps.v16,
                topGap: Gaps.v80,
                subject: "Follow the rules",
                subtitle: "Take care of one another! plis!",
              ),
              crossFadeState: _showingPage == Page.first
                  ? CrossFadeState.showFirst
                  : CrossFadeState.showSecond,
              duration: const Duration(milliseconds: 300),
            ),
          ),
        ),
        bottomNavigationBar: Container(
          color: isDarkMode(context) ? Colors.black : Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(
              top: Sizes.size32,
              bottom: Sizes.size64,
              left: Sizes.size24,
              right: Sizes.size24,
            ),
            child: AnimatedOpacity(
              opacity: _showingPage == Page.first ? 0 : 1,
              duration: const Duration(milliseconds: 200),
              child: CupertinoButton(
                onPressed: _onEntertheAppTap,
                color: Theme.of(context).primaryColor,
                child: const Text("Enterthe the app!"),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class tutorialPage extends StatelessWidget {
  final SizedBox gap;
  final SizedBox topGap;
  final String subject;
  final String subtitle;

  const tutorialPage({
    super.key,
    required this.gap,
    required this.subject,
    required this.subtitle,
    required this.topGap,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        topGap,
        Text(
          subject, //"Watch cool videos!",
          style: const TextStyle(
            fontSize: Sizes.size36,
            fontWeight: FontWeight.bold,
          ),
        ),
        gap,
        Text(
          subtitle, //"Videos are personalized for you based on what you watch, like, and share.",
          style: const TextStyle(
            fontSize: Sizes.size16,
          ),
        ),
      ],
    );
  }
}
