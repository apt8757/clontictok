import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tiktok_clone/constants/breakpoints.dart';
import 'package:tiktok_clone/constants/gaps.dart';
import 'package:tiktok_clone/constants/sizes.dart';
import 'package:tiktok_clone/features/settings/settings_screen.dart';
import 'package:tiktok_clone/features/users/user_update_info_screen.dart';
import 'package:tiktok_clone/features/users/view_models/user_view_model.dart';
import 'package:tiktok_clone/features/users/widgets/avarta.dart';
import 'package:tiktok_clone/features/users/widgets/persistent_tab_bar.dart';
import 'package:tiktok_clone/features/users/widgets/user_account.dart';

class UserProfileScreen extends ConsumerStatefulWidget {
  const UserProfileScreen({super.key});

  @override
  ConsumerState<UserProfileScreen> createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends ConsumerState<UserProfileScreen> {
  void _onGearPressed() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => const SettingsScreen(),
      ),
    );
  }

  void _onUserPenPressed(String bio, String link) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => UserUpdateInfoScreen(bio: bio, link: link),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    // final isDark = isDarkMode(context);
    return ref.watch(usersProvider).when(
          error: (error, stackTrace) => Center(
            child: Text(
              error.toString(),
            ),
          ),
          loading: () => const Center(
            child: CircularProgressIndicator.adaptive(),
          ),
          data: (data) => Scaffold(
            backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
            body: SafeArea(
              child: DefaultTabController(
                length: 2,
                child: NestedScrollView(
                  headerSliverBuilder: (context, innerBoxIsScrolled) {
                    return [
                      SliverAppBar(
                        centerTitle: true,
                        title: Text(data.name),
                        actions: [
                          IconButton(
                            onPressed: () =>
                                _onUserPenPressed(data.bio, data.link),
                            icon: const FaIcon(
                              FontAwesomeIcons.userPen,
                              size: Sizes.size20,
                            ),
                          ),
                          IconButton(
                            onPressed: _onGearPressed,
                            icon: const FaIcon(
                              FontAwesomeIcons.gear,
                              size: Sizes.size20,
                            ),
                          ),
                        ],
                      ),
                      SliverToBoxAdapter(
                        child: size.width < Breakpoints.md
                            ? Column(
                                children: [
                                  Gaps.v20,
                                  Avartar(
                                    uid: data.uid,
                                    name: data.name,
                                    hasAvatar: data.hasAvatar,
                                    isUpload: false,
                                  ),
                                  Gaps.v20,
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "@${data.name}",
                                        style: const TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: Sizes.size16,
                                        ),
                                      ),
                                      Gaps.h5,
                                      FaIcon(
                                        FontAwesomeIcons.solidCircleCheck,
                                        size: Sizes.size16,
                                        color: Colors.blue.shade500,
                                      ),
                                    ],
                                  ),
                                  Gaps.v24,
                                  SizedBox(
                                    height: Sizes.size52,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const UserAccount(
                                          title: "37",
                                          subtitle: "Following",
                                        ),
                                        VerticalDivider(
                                          width: Sizes.size32,
                                          thickness: Sizes.size1,
                                          color: Colors.grey.shade400,
                                          indent: Sizes.size14,
                                          endIndent: Sizes.size14,
                                        ),
                                        const UserAccount(
                                          title: "10.5M",
                                          subtitle: "Followers",
                                        ),
                                        VerticalDivider(
                                          width: Sizes.size32,
                                          thickness: Sizes.size1,
                                          color: Colors.grey.shade400,
                                          indent: Sizes.size14,
                                          endIndent: Sizes.size14,
                                        ),
                                        const UserAccount(
                                          title: "149.3M",
                                          subtitle: "Likes",
                                        ),
                                      ],
                                    ),
                                  ),
                                  Gaps.v14,
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Flexible(
                                        child: FractionallySizedBox(
                                          widthFactor: 0.65,
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                              vertical: Sizes.size12,
                                            ),
                                            decoration: BoxDecoration(
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(
                                                  Sizes.size4,
                                                ),
                                              ),
                                            ),
                                            child: const Text(
                                              "Follow",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Gaps.h4,
                                      Container(
                                        width: 43,
                                        height: 43,
                                        padding: const EdgeInsets.symmetric(
                                            // vertical: Sizes.size9,
                                            // horizontal: Sizes.size9,
                                            ),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.grey.shade300,
                                          ),
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(
                                              Sizes.size4,
                                            ),
                                          ),
                                        ),
                                        child: const Center(
                                          child: FaIcon(
                                            FontAwesomeIcons.youtube,
                                          ),
                                        ),
                                      ),
                                      Gaps.h4,
                                      Container(
                                        width: 43,
                                        height: 43,
                                        padding: const EdgeInsets.symmetric(
                                            // vertical: Sizes.size9,
                                            // horizontal: Sizes.size14,
                                            ),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.grey.shade300,
                                          ),
                                          borderRadius: const BorderRadius.all(
                                            Radius.circular(
                                              Sizes.size4,
                                            ),
                                          ),
                                        ),
                                        child: const Center(
                                          child: FaIcon(
                                            FontAwesomeIcons.caretDown,
                                            // size: Sizes.size16,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Gaps.v14,
                                  const Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: Sizes.size32,
                                    ),
                                    child: Text(
                                      "All highlights and where to watch live matches on FIFA+",
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Gaps.v14,
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      FaIcon(
                                        FontAwesomeIcons.link,
                                        size: Sizes.size12,
                                      ),
                                      Gaps.h4,
                                      Text(
                                        "https://www.fifa.com/fifaplus/en/home",
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Gaps.v20,
                                ],
                              )
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    children: [
                                      CircleAvatar(
                                        radius: 70,
                                        foregroundImage: const NetworkImage(
                                            "https://secure.gravatar.com/avatar/a7a4490fc6fd768832a922b4465863cc?s=800&d=identicon"),
                                        child: Text(
                                          data.name,
                                        ),
                                      ),
                                      Gaps.h20,
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "@${data.name}",
                                            style: const TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: Sizes.size16,
                                            ),
                                          ),
                                          Gaps.h5,
                                          FaIcon(
                                            FontAwesomeIcons.solidCircleCheck,
                                            size: Sizes.size16,
                                            color: Colors.blue.shade500,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Gaps.h24,
                                  Column(
                                    children: [
                                      SizedBox(
                                        height: Sizes.size48,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            const UserAccount(
                                              title: "37",
                                              subtitle: "Following",
                                            ),
                                            VerticalDivider(
                                              width: Sizes.size32,
                                              thickness: Sizes.size1,
                                              color: Colors.grey.shade400,
                                              indent: Sizes.size14,
                                              endIndent: Sizes.size14,
                                            ),
                                            const UserAccount(
                                              title: "10.5M",
                                              subtitle: "Following",
                                            ),
                                            VerticalDivider(
                                              width: Sizes.size32,
                                              thickness: Sizes.size1,
                                              color: Colors.grey.shade400,
                                              indent: Sizes.size14,
                                              endIndent: Sizes.size14,
                                            ),
                                            const UserAccount(
                                              title: "149.3M",
                                              subtitle: "Likes",
                                            ),
                                          ],
                                        ),
                                      ),
                                      Gaps.v14,
                                      SizedBox(
                                        width: 450,
                                        child: FractionallySizedBox(
                                          widthFactor: 0.65,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Flexible(
                                                flex: 4,
                                                fit: FlexFit.tight,
                                                child: Container(
                                                  padding: const EdgeInsets
                                                      .symmetric(
                                                    vertical: Sizes.size12,
                                                  ),
                                                  decoration: BoxDecoration(
                                                    color: Theme.of(context)
                                                        .primaryColor,
                                                    borderRadius:
                                                        const BorderRadius.all(
                                                      Radius.circular(
                                                        Sizes.size4,
                                                      ),
                                                    ),
                                                  ),
                                                  child: const Text(
                                                    "Follow",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ),
                                              Gaps.h4,
                                              Flexible(
                                                flex: 1,
                                                fit: FlexFit.tight,
                                                child: Container(
                                                  width: 43,
                                                  height: 43,
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      // vertical: Sizes.size9,
                                                      // horizontal: Sizes.size9,
                                                      ),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color:
                                                          Colors.grey.shade300,
                                                    ),
                                                    borderRadius:
                                                        const BorderRadius.all(
                                                      Radius.circular(
                                                        Sizes.size4,
                                                      ),
                                                    ),
                                                  ),
                                                  child: const Center(
                                                    child: FaIcon(
                                                      FontAwesomeIcons.youtube,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Gaps.h4,
                                              Flexible(
                                                flex: 1,
                                                fit: FlexFit.tight,
                                                child: Container(
                                                  width: 43,
                                                  height: 43,
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      // vertical: Sizes.size9,
                                                      // horizontal: Sizes.size14,
                                                      ),
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color:
                                                          Colors.grey.shade300,
                                                    ),
                                                    borderRadius:
                                                        const BorderRadius.all(
                                                      Radius.circular(
                                                        Sizes.size4,
                                                      ),
                                                    ),
                                                  ),
                                                  child: const Center(
                                                    child: FaIcon(
                                                      FontAwesomeIcons
                                                          .caretDown,
                                                      // size: Sizes.size16,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Gaps.v14,
                                      const Padding(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: Sizes.size32,
                                        ),
                                        child: Text(
                                          "All highlights and where to watch live matches on FIFA+",
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Gaps.v14,
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: const [
                                          FaIcon(
                                            FontAwesomeIcons.link,
                                            size: Sizes.size12,
                                          ),
                                          Gaps.h4,
                                          Text(
                                            "https://www.fifa.com/fifaplus/en/home",
                                            style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Gaps.v20,
                                    ],
                                  ),
                                ],
                              ),
                      ),
                      SliverPersistentHeader(
                        delegate: PersistentTabBar(),
                        pinned: true,
                      ),
                    ];
                  },
                  body: TabBarView(
                    children: [
                      GridView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        keyboardDismissBehavior:
                            ScrollViewKeyboardDismissBehavior.onDrag,
                        itemCount: 20,
                        padding: EdgeInsets.zero,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: size.width > Breakpoints.sm ? 5 : 3,
                          crossAxisSpacing: Sizes.size2,
                          mainAxisSpacing: Sizes.size2,
                          childAspectRatio: 9 / 14,
                        ),
                        itemBuilder: (context, index) => Column(
                          children: [
                            Stack(
                              children: [
                                AspectRatio(
                                  aspectRatio: 9 / 14,
                                  child: FadeInImage.assetNetwork(
                                      fit: BoxFit.cover,
                                      placeholder: "assets/images/loading.gif",
                                      image:
                                          "https://images.unsplash.com/photo-1555126634-323283e090fa?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1364&q=80"),
                                ),
                                Positioned(
                                  bottom: 0,
                                  child: Row(
                                    children: const [
                                      FaIcon(
                                        Icons.play_arrow_outlined,
                                        color: Colors.white,
                                        size: Sizes.size24,
                                      ),
                                      Gaps.h2,
                                      Text(
                                        "4.1M",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: Sizes.size14,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const Center(
                        child: Text("Page one"),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
  }
}



// SliverAppBar 연습

//         SliverAppBar(
//           // floating: true,
//           stretch: true,
//           pinned: true,
//           collapsedHeight: 80,
//           expandedHeight: 200,
//           backgroundColor: Theme.of(context).primaryColor,
//           flexibleSpace: FlexibleSpaceBar(
//             stretchModes: const [
//               StretchMode.blurBackground,
//               StretchMode.zoomBackground,
//             ],
//             background: Image.asset(
//               "assets/images/sample.jpg",
//               fit: BoxFit.cover,
//             ),
//             title: const Text("안녕!"),
//           ),
//         ),
//         SliverFixedExtentList(
//           delegate: SliverChildBuilderDelegate(
//             childCount: 50,
//             (context, index) => Container(
//               color: Colors.amber[100 * (index % 9)],
//               child: Align(
//                 alignment: Alignment.center,
//                 child: Text(
//                   "item $index",
//                 ),
//               ),
//             ),
//           ),
//           itemExtent: 100,
//         ),
//         SliverPersistentHeader(
//           delegate: CustomDelegate(),
//           pinned: true,
//         ),
//         SliverGrid(
//           delegate: SliverChildBuilderDelegate(
//             (context, index) => Container(
//               color: Colors.blue[100 * (index % 9)],
//               child: Align(
//                 alignment: Alignment.center,
//                 child: Text(
//                   "item $index",
//                 ),
//               ),
//             ),
//           ),
//           gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
//             mainAxisSpacing: Sizes.size20,
//             crossAxisSpacing: Sizes.size20,
//             maxCrossAxisExtent: 100,
//             childAspectRatio: 1,
//           ),
//         )

// class CustomDelegate extends SliverPersistentHeaderDelegate {
//   @override
//   Widget build(
//       BuildContext context, double shrinkOffset, bool overlapsContent) {
//     return Container(
//       color: Colors.indigo,
//       child: const FractionallySizedBox(
//         heightFactor: 1,
//         child: Center(
//           child: Text(
//             "Title !!",
//             style: TextStyle(
//               color: Colors.white,
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   @override
//   double get maxExtent => 100;

//   @override
//   double get minExtent => 100;

//   @override
//   bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
//     return false;
//   }
// }
