import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:tiktok_clone/constants/gaps.dart';
import 'package:tiktok_clone/constants/sizes.dart';
import 'package:tiktok_clone/features/users/view_models/user_update_view_model.dart';
import 'package:tiktok_clone/features/users/view_models/user_view_model.dart';
import 'package:tiktok_clone/features/users/widgets/avarta.dart';

class UserUpdateInfoScreen extends ConsumerStatefulWidget {
  final String bio;
  final String link;
  const UserUpdateInfoScreen({
    super.key,
    required this.bio,
    required this.link,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _UserUpdateInfoScreenState();
}

class _UserUpdateInfoScreenState extends ConsumerState<UserUpdateInfoScreen> {
  late final TextEditingController _BirthdayController;
  late final TextEditingController _textEditingController;

  DateTime? tempPickedDate;
  late DateTime _selectedDate = DateTime.parse(widget.bio);
  DateTime initialDate =
      DateTime.now().subtract(const Duration(days: (365 * 12)));

  @override
  void dispose() {
    _BirthdayController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _BirthdayController = TextEditingController(text: widget.bio);
    _textEditingController = TextEditingController(text: widget.link);
  }

  void _onUserProdfileUpdatePressed(String type, String value) {
    // print("type ==> $type");
    // print("value ==> $value");
    ref.read(userUpdateProvider.notifier).userProfileUpdate(type, value);
  }

  void _onScaffoldTap() {
    FocusScope.of(context).unfocus();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return ref.watch(usersProvider).when(
          loading: () => const Center(
            child: CircularProgressIndicator.adaptive(),
          ),
          error: (error, stackTrace) => Center(
            child: Text(
              error.toString(),
            ),
          ),
          data: (data) => GestureDetector(
            onTap: _onScaffoldTap,
            child: Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: const Text('Edit profile'),
              ),
              body: Container(
                width: size.width,
                alignment: Alignment.topCenter,
                margin: const EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Column(
                  children: [
                    Gaps.v20,
                    Avartar(
                      uid: data.uid,
                      name: data.name,
                      hasAvatar: data.hasAvatar,
                      isUpload: true,
                    ),
                    Gaps.v5,
                    const Text(
                      "photo update",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        HapticFeedback.mediumImpact();
                        _selectDate(data.bio);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const SizedBox(
                            width: 70,
                            child: Text(
                              'Bio',
                              style: TextStyle(
                                  fontSize: Sizes.size24,
                                  color: Colors.blueGrey),
                            ),
                          ),
                          Expanded(
                            child: TextFormField(
                              enabled: false,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                isDense: true,
                              ),
                              controller: _BirthdayController,
                              style: const TextStyle(fontSize: 20),
                            ),
                          ),
                          Gaps.h20,
                          TextButton(
                            onPressed: () => _onUserProdfileUpdatePressed(
                              "1",
                              _BirthdayController.text,
                            ),
                            child: Text(
                              '등록',
                              style: TextStyle(
                                fontSize: Sizes.size24,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Gaps.v20,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const SizedBox(
                          width: 70,
                          child: Text('Link',
                              style: TextStyle(
                                  fontSize: Sizes.size24,
                                  color: Colors.blueGrey)),
                        ),
                        Expanded(
                          child: TextFormField(
                            controller: _textEditingController,
                            cursorColor: Theme.of(context).primaryColor,
                            decoration: const InputDecoration(
                              border: InputBorder.none,
                              labelStyle: TextStyle(
                                fontSize: Sizes.size24,
                              ),
                              // enabledBorder: UnderlineInputBorder(
                              //   borderSide: BorderSide(
                              //     color: Colors.grey.shade400,
                              //   ),
                              // ),
                              // focusedBorder: UnderlineInputBorder(
                              //   borderSide: BorderSide(
                              //     color: Colors.grey.shade400,
                              //   ),
                              // ),
                            ),
                            style: const TextStyle(
                              fontSize: 20,
                              decorationThickness: 0,
                            ),
                          ),
                        ),
                        Gaps.h20,
                        TextButton(
                          onPressed: () => _onUserProdfileUpdatePressed(
                            "2",
                            _textEditingController.text,
                          ),
                          child: Text(
                            '등록',
                            style: TextStyle(
                              fontSize: Sizes.size24,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Gaps.v20,
                  ],
                ),
              ),
            ),
          ),
        );
  }

  _selectDate(String bio) async {
    DateTime? pickedDate = await showModalBottomSheet<DateTime>(
      backgroundColor: ThemeData.light().scaffoldBackgroundColor,
      context: context,
      builder: (context) {
        // DateTime tempPickedDate;
        return SizedBox(
          height: 300,
          child: Column(
            children: <Widget>[
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    CupertinoButton(
                      child: const Text('취소'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    CupertinoButton(
                      child: const Text('완료'),
                      onPressed: () {
                        Navigator.of(context).pop(tempPickedDate);
                        FocusScope.of(context).unfocus();
                      },
                    ),
                  ],
                ),
              ),
              const Divider(
                height: 0,
                thickness: 1,
              ),
              Expanded(
                child: Container(
                  child: CupertinoDatePicker(
                    backgroundColor: ThemeData.light().scaffoldBackgroundColor,
                    initialDateTime: _selectedDate,
                    maximumDate: initialDate,
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (DateTime dateTime) {
                      tempPickedDate = dateTime;
                    },
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );

    if (pickedDate != null && pickedDate != _selectedDate) {
      setState(() {
        _selectedDate = pickedDate;
        _BirthdayController.text = pickedDate.toString();
        convertDateTimeDisplay(_BirthdayController.text);
      });
    }
  }

  String convertDateTimeDisplay(String date) {
    final DateFormat displayFormater = DateFormat('yyyy-MM-dd HH:mm:ss.SSS');
    final DateFormat serverFormater = DateFormat('yyyy-MM-dd');
    final DateTime displayDate = displayFormater.parse(date);
    return _BirthdayController.text = serverFormater.format(displayDate);
  }
}
