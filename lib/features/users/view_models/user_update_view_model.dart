import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tiktok_clone/features/authentication/repos/authentication_repo.dart';
import 'package:tiktok_clone/features/users/repos/user_repository.dart';
import 'package:tiktok_clone/features/users/view_models/user_view_model.dart';

class UserUpdateViewModel extends AsyncNotifier<void> {
  late final UserRepository _repository;

  @override
  FutureOr<void> build() {
    _repository = ref.read(userRepo);
  }

  Future<void> userProfileUpdate(String type, String value) async {
    state = const AsyncValue.loading();
    final uid = ref.read(authRepo).user!.uid;
    state = await AsyncValue.guard(() async => await ref
        .read(usersProvider.notifier)
        .onProfileUpload(type, uid, value));
  }
}

final userUpdateProvider = AsyncNotifierProvider<UserUpdateViewModel, void>(
  () => UserUpdateViewModel(),
);
