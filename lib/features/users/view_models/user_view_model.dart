import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tiktok_clone/features/authentication/repos/authentication_repo.dart';
import 'package:tiktok_clone/features/authentication/view_models/signup_view_model.dart';
import 'package:tiktok_clone/features/users/models/user_profile_model.dart';
import 'package:tiktok_clone/features/users/repos/user_repository.dart';

class UsersViewModel extends AsyncNotifier<UserProfileModel> {
  late final UserRepository _userRepository;
  late final AuthenticationRepository _authenticationRepository;

  @override
  FutureOr<UserProfileModel> build() async {
    // await Future.delayed(const Duration(seconds: 10));

    _userRepository = ref.read(userRepo);
    _authenticationRepository = ref.read(authRepo);

    if (_authenticationRepository.isLoggedIn) {
      final profile = await _userRepository
          .findProfile(_authenticationRepository.user!.uid);
      if (profile != null) {
        return UserProfileModel.fromJson(profile);
      }
    }

    return UserProfileModel.empty();
  }

  Future<void> createProfile(UserCredential credential) async {
    final form = ref.read(signUpForm);
    state = const AsyncValue.loading();
    final profile = UserProfileModel(
      bio: form["bio"] ?? "미등록",
      link: "미등록",
      email: credential.user!.email ?? "미등록",
      name: form["name"] ?? "익명",
      uid: credential.user!.uid,
      hasAvatar: false,
    );
    await _userRepository.createProfile(profile);
    state = AsyncValue.data(profile);
  }

  Future<void> onAvatarUpload() async {
    state = AsyncValue.data(state.value!.copyWith(hasAvatar: true));
    await _userRepository.updateUser(state.value!.uid, {"hasAvatar": true});
  }

  Future<void> onProfileUpload(String type, String uid, String value) async {
    switch (type) {
      case "1":
        state = AsyncValue.data(state.value!.copyWith(bio: value));
        await _userRepository.updateUser(state.value!.uid, {"bio": value});
        break;
      case "2":
        state = AsyncValue.data(state.value!.copyWith(link: value));
        await _userRepository.updateUser(state.value!.uid, {"link": value});
        break;
      default:
    }
  }

  Future<UserProfileModel> getUser() async {
    return state.value!.copyWith();
  }
}

final usersProvider = AsyncNotifierProvider<UsersViewModel, UserProfileModel>(
  () => UsersViewModel(),
);
