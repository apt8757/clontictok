class UserProfileModel {
  final String uid;
  final String name;
  final String email;
  final String bio;
  final String link;
  final bool hasAvatar;

  UserProfileModel({
    required this.uid,
    required this.email,
    required this.name,
    required this.bio,
    required this.link,
    required this.hasAvatar,
  });

  UserProfileModel.empty()
      : uid = "",
        email = "",
        name = "",
        link = "",
        bio = "",
        hasAvatar = false;

  UserProfileModel.fromJson(Map<String, dynamic> json)
      : uid = json["uid"],
        email = json["email"],
        name = json["name"],
        link = json["link"],
        bio = json["bio"],
        hasAvatar = json["hasAvatar"];

  Map<String, dynamic> toJson() {
    return {
      "uid": uid,
      "email": email,
      "name": name,
      "link": link,
      "bio": bio,
      "hasAvatar": hasAvatar,
    };
  }

  UserProfileModel copyWith({
    String? uid,
    String? name,
    String? email,
    String? bio,
    String? link,
    bool? hasAvatar,
  }) {
    return UserProfileModel(
      bio: bio ?? this.bio,
      email: email ?? this.email,
      link: link ?? this.link,
      name: name ?? this.name,
      uid: uid ?? this.uid,
      hasAvatar: hasAvatar ?? this.hasAvatar,
    );
  }
}
