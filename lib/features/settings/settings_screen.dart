import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:go_router/go_router.dart';
import 'package:tiktok_clone/common/dark_mode/view_models/dark_mode_vm.dart';
import 'package:tiktok_clone/features/authentication/repos/authentication_repo.dart';
import 'package:tiktok_clone/features/videos/view_models/playback_config_vm.dart';

class SettingsScreen extends ConsumerWidget {
  const SettingsScreen({super.key});

  // bool _notifications = false;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: ListView(
        children: [
          SwitchListTile.adaptive(
            value: ref.watch(darkModeProvider).isDarkMode,
            onChanged: (value) {
              ref.read(darkModeProvider.notifier).setDarkMode(value);
            },
            title: const Text("Dark Mode"),
            subtitle: const Text("Light mode is the default."),
          ),
          SwitchListTile.adaptive(
            value: ref.watch(playbackConfigProvider).autoplay,
            onChanged: (value) =>
                ref.read(playbackConfigProvider.notifier).setAutoplay(value),
            // context.read<PlaybackConfigVeiwModel>().setAutoplay(value);
            title: const Text("Autoplay"),
            subtitle: const Text("Videos will start playing automatically"),
          ),
          SwitchListTile.adaptive(
            value: ref.watch(playbackConfigProvider).muted,
            onChanged: (value) =>
                ref.read(playbackConfigProvider.notifier).setMuted(value),
            // context.read<PlaybackConfigVeiwModel>().setMuted(value);
            title: const Text("Mute video"),
            subtitle: const Text("Videos will be muted by default."),
          ),
          SwitchListTile.adaptive(
            value: false,
            onChanged: (value) => {},
            title: const Text("Enable notifications"),
            subtitle: const Text("Enable notifications"),
          ),
          CheckboxListTile(
            activeColor: Colors.black,
            value: false,
            onChanged: (value) => {},
            title: const Text("Enable notifications"),
          ),
          ListTile(
            onTap: () async {
              final date = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(1980),
                lastDate: DateTime(2030),
              );
              if (kDebugMode) {
                print(date);
              }

              final time = await showTimePicker(
                context: context,
                initialTime: TimeOfDay.now(),
              );

              if (kDebugMode) {
                print(time);
              }

              final booking = await showDateRangePicker(
                context: context,
                firstDate: DateTime(1980),
                lastDate: DateTime(2030),
                builder: (context, child) {
                  return Theme(
                    data: ThemeData(
                        appBarTheme: const AppBarTheme(
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.black)),
                    child: child!,
                  );
                },
              );
              if (kDebugMode) {
                print(booking);
              }
            },
            title: const Text("What is your birthday?"),
            subtitle: const Text("I need to know!"),
          ),
          ListTile(
            title: const Text("Log out (iOS)"),
            textColor: Colors.red,
            onTap: () {
              showCupertinoDialog(
                context: context,
                builder: (context) => CupertinoAlertDialog(
                  title: const Text("Are you sure?"),
                  content: const Text("Plx dont go"),
                  actions: [
                    CupertinoDialogAction(
                      onPressed: () => Navigator.of(context).pop(),
                      child: const Text("No"),
                    ),
                    CupertinoDialogAction(
                      onPressed: () {
                        ref.read(authRepo).signOut();
                        context.go("/");
                      },
                      isDestructiveAction: true,
                      child: const Text("Yes"),
                    ),
                  ],
                ),
              );
            },
          ),
          ListTile(
            title: const Text("Log out (Android)"),
            textColor: Colors.red,
            onTap: () {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  icon: const FaIcon(FontAwesomeIcons.skull),
                  title: const Text("Are you sure?"),
                  content: const Text("Plx dont go"),
                  actions: [
                    IconButton(
                      onPressed: () => Navigator.of(context).pop(),
                      icon: const FaIcon(FontAwesomeIcons.car),
                    ),
                    TextButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: const Text("Yes"),
                    ),
                  ],
                ),
              );
            },
          ),
          ListTile(
            title: const Text("Log out (iOS / Bottom)"),
            textColor: Colors.red,
            onTap: () {
              showCupertinoModalPopup(
                context: context,
                builder: (context) => CupertinoActionSheet(
                  title: const Text("Are you sure?"),
                  message: const Text("Please dooooont gooooo"),
                  actions: [
                    CupertinoActionSheetAction(
                      isDefaultAction: true,
                      onPressed: () => Navigator.of(context).pop(),
                      child: const Text("Not log out"),
                    ),
                    CupertinoActionSheetAction(
                      isDestructiveAction: true,
                      onPressed: () => Navigator.of(context).pop(),
                      child: const Text("Yes plz."),
                    )
                  ],
                ),
              );
            },
          ),
          const AboutListTile(
            applicationVersion: "1.0",
            applicationLegalese: "Don't copy me.",
          ),
        ],
      ),
    );
  }
}

// CupertinoActivityIndicator(), // IOS 로딩바
// CircularProgressIndicator(), // AOS 로딩바
// CircularProgressIndicator.adaptive(),  // 사용중인 OS에 맞는 로딩바 표출

// 라이센스 커스텀마이징
// ListTile(
//   onTap: () => showAboutDialog(
//     context: context,
//     applicationVersion: "1.0",
//     applicationLegalese: "All rights reseverd. Please dont copy me.",
//   ),
//   title: const Text(
//     "About",
//     style: TextStyle(
//       fontWeight: FontWeight.w600,
//     ),
//   ),
//   subtitle: const Text("About this app..."),
// ),

// 휠 리스트

// ListWheelScrollView(
//         diameterRatio: 1.5, // 지름
//         // offAxisFraction: 3, // 중심선
//         itemExtent: 200, // 아이템 크기
//         children: [
//           for (var x in [1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1])
//             FractionallySizedBox(
//               widthFactor: 1,
//               child: Container(
//                 color: Colors.teal,
//                 alignment: Alignment.center,
//                 child: const Text(
//                   "!!",
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 39,
//                   ),
//                 ),
//               ),
//             ),
//         ],
//       ),


// 켈린더 
// ListTile(
//   onTap: () async {
//     final date = await showDatePicker(
//       context: context,
//       initialDate: DateTime.now(),
//       firstDate: DateTime(1980),
//       lastDate: DateTime(2030),
//     );
//     print(date);
//     final time = await showTimePicker(
//       context: context,
//       initialTime: TimeOfDay.now(),
//     );
//     print(time);
//     final booking = await showDateRangePicker(
//       context: context,
//       firstDate: DateTime(1980),
//       lastDate: DateTime(2030),
//       builder: (context, child) {
//         return Theme(
//           data: ThemeData(
//               appBarTheme: const AppBarTheme(
//                   foregroundColor: Colors.white,
//                   backgroundColor: Colors.black)),
//           child: child!,
//         );
//       },
//     );
//     print(booking);
//   },
//   title: const Text("What is your birthday?"),
// ),