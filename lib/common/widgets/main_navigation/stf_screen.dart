
import 'package:flutter/material.dart';

class stfScreen extends StatefulWidget {
  const stfScreen({super.key});

  @override
  State<stfScreen> createState() => _stfScreenState();
}

class _stfScreenState extends State<stfScreen> {
  int cnt = 0;

  void _cntAdd() {
    setState(() {
      cnt++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        transformAlignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "$cnt",
              style: const TextStyle(
                fontSize: 30,
              ),
            ),
            TextButton(
              onPressed: _cntAdd,
              child: const Text("+"),
            )
          ],
        ),
      ),
    );
  }
}
