import 'package:flutter/material.dart';

class VideoConfig extends ChangeNotifier {
  bool isMute = false;
  bool isAutoPlay = false;

  void toggleIsMute() {
    isMute = !isMute;
    notifyListeners();
  }

  void toggleAutoPlay() {
    isAutoPlay = !isAutoPlay;
    notifyListeners();
  }
}

final videoConfig = VideoConfig();
