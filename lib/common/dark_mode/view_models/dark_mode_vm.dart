import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tiktok_clone/common/dark_mode/models/dark_mode_model.dart';
import 'package:tiktok_clone/common/dark_mode/repos/dark_mode_repo.dart';

class DarkModeConfig extends Notifier<DarkMode> {
  final DarkModeRepository _repository;

  DarkModeConfig(this._repository);

  void setDarkMode(value) {
    _repository.setDarkMode(value);
    state = DarkMode(
      isDarkMode: value,
    );
  }

  @override
  DarkMode build() {
    return DarkMode(
      isDarkMode: _repository.isDarkMode(),
    );
  }
}

final darkModeProvider = NotifierProvider<DarkModeConfig, DarkMode>(
  () => throw UnimplementedError(),
);
